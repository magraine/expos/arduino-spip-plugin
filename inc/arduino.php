<?php

define('_ARDUINO_DEFAULT_LOCK_FILE', _DIR_TMP . 'arduino.lock');

class arduino {
	var $serialPort = "/dev/ttyUSB0";
	var $speed = 9600;
	var $con; // class phpSerial
	var $useLock = false; // si oui, un fichier de lock est cree le temps du traitement
	var $lockFile = _ARDUINO_DEFAULT_LOCK_FILE;
	
	// constructeur
	function __construct($params = array()) {
		return $this->arduino($params);
	}


	// initialisation des donnees
	// on peut passer un tableau array('speed'=>56000, 'arg'=>'valeur');
	function arduino($params = array()) {
		if (is_array($params)) {
			foreach ($params as $p=>$v) {
				if (isset($this->$p)) {
					$this->$p = $v;
				}
			}
		}
	}


	// ouvrir une connexion serie avec arduino
	function open() {
		include_spip("lib/php_serial.class");

		// Let's start the class
		$this->con = new phpSerial;

		// First we must specify the device.
		$this->con->deviceSet($this->serialPort);

		// valeur de debit
		$this->con->confBaudRate($this->speed);

		// Then we need to open it
		if ($this->waitForUnlock())
			return $this->con->deviceOpen();
		else // oups
			return false;
	}


	// fermer la connexion
	function close() {
		$this->con->deviceClose();
		
	}


	// lire si on est libre
	function locked() {
		if (!$this->useLock OR file_exists($this->lockFile));
	}
	
	// bloquer
	function lock() {
		if (!$this->useLock)
			return file_put_contents($this->lockFile, time());
		else
			return false;
	}
	
	// liberer
	function unlock() {
		if (!$this->useLock)
			return unlink($this->lockFile);
		else
			return false;			
	}	
	
	// attendre qu'on soit debloque
	function waitForUnlock() {
		if (!$this->locked()) return true;
		$tour = 0;
		while ($this->locked()) {
			if (++$tour == 10) return false;
			usleep(100);
		}
		return true;
	}
	
	// envoyer un message
	function send($message) {
		if ($this->waitForUnlock()) {
			$this->lock();
			$res = $this->con->sendMessage($message);
			$this->unlock();
			return $res;
		} else {
			return false;
		}
	}
	
	// recevoir un message
	function read() {
		if ($this->waitForUnlock()) {
			$this->lock();
			$res = $this->con->readPort();
			$this->unlock();
			return $res;			
		} else {
			return false;
		}		
	}

	// envoyer puis revecoir un message
	// recevoir un message
	function sendAndRead($message) {
		if ($this->waitForUnlock()) {
			$this->lock();
			$this->con->sendMessage($message);
			$res = $this->con->readPort();
			$this->unlock();
			return $res;			
		} else {
			return false;
		}		
	}	

	
	function analogReadAll() {
		$analogs = "";
		if ($data = $this->sendAndRead("RA;")) {
			// $data = "(120,0,123)";
			eval('$analogs = array' . $data . ';'); // retourne un tableau $analogs
		}
		
		if (is_array($analogs))
			return $analogs;
		else
			return false;
	}

	function analogRead($pin) {
		$data = $this->analogReadAll();
		if (is_array($data)) {
			return $data[$pin];
		}
		return $false;
	}

	function digitalReadAll() {
		$analogs = "";
		if ($data = $this->sendAndRead("RD;")) {
			// $data = "(120,0,123)";
			eval('$digitals = array' . $data . ';'); // retourne un tableau $digitals
		}
		
		if (is_array($digitals))
			return $digitals;
		else
			return false;
	}

	function digitalRead($pin) {
		$data = $this->digitalReadAll();
		if (is_array($data)) {
			return $data[$pin];
		}
		return $false;
	}	

	function digitalWrite($pin, $value) {
		$this->send("WD$pin:$value;");
	}


	function analogWrite($pin, $value) {
		$this->send("WA$pin:$value;");
	}	
}
?>
