#include <WString.h>

int val;

String buff = "";
char NEWLINE = ';';


int LED_OUT = 13;
int in_pins[] = {2,3,4,5,6};
int out_pins[] = {8,9,10,11,12};
int nPins = 5;

void setup()
{
  Serial.begin(115200);
  
  int i;
  pinMode(LED_OUT, OUTPUT);
  for (i = 0; i<nPins; i++) {
    pinMode(out_pins[i], OUTPUT);
    pinMode(in_pins[i], INPUT);
  }
}

void loop()
{
  if (Serial.available() > 0) {
    serialEvent(Serial.read());
  }
  digitalWrite(LED_OUT, digitalRead(12));
}


void serialEvent(int serial) 
{

  // on lit chaque ligne envoyees
  char letter = char(serial);
  if(letter != NEWLINE) { 
    buff += letter;
    // Serial.println(buff);
  } else {
  //  Serial.println("----");
  //  Serial.println(buff);
    // Au moment de la fin de la ligne, on analyse ce qui a ete transmis
    // on recupere le premier caractere qui est le code de ce qui va etre realise
    char quoi = buff.charAt(0); // W ou R (write or read) 
    buff = buff.substring(1);


    // WD3:130
    if (quoi == 'W') {
      executeWrite(buff);
    }
    
    // RA
    if (quoi == 'R') {
      executeRead(buff); 
    }
      
    // remise a zero de la ligne
    buff = "";
  }
}

void executeWrite(String phrase) {
    
      char qui = phrase.charAt(0); // A ou D (analogic or decimal)
      phrase = phrase.substring(1);
      
      int pin = phrase.charAt(0)-48; // "48" = 0
      phrase = phrase.substring(1);
      
      if (phrase.charAt(0) != ':') {
        pin = 10*pin + phrase.charAt(0)-48;
        phrase = phrase.substring(1);
      }
      // on enleve les ':'
      phrase = phrase.substring(1);
      int value = 0;
      int i;
      for (i=0; i<phrase.length(); i++) {
        value = 10*value+phrase.charAt(i)-48;
      }
      // analog
      if (qui == 'A') {
         if (pin>=0 && pin<14) {
            analogWrite(pin, value);
         }
      }   
      // digital
      if (qui == 'D') {
         if (pin>=0 && pin<14) {
            digitalWrite(pin, value);
         }
      }  
}

void executeRead(String phrase) {
      char qui = phrase.charAt(0); // A ou D (analogic or decimal)
      phrase = phrase.substring(1);
      
      int datas[14];
      
      // analog
      if (qui == 'A') {
          // output all pin value as php array
          for (int i = 0; i < 6; i++) {
            datas[i] = analogRead(i);
          }
          outputArray(datas,6);
      }
       // digital
      if (qui == 'D') {
          for (int i = 0; i < 14; i++) {
            datas[i] = digitalRead(i);
          }
          outputArray(datas,14);
      }
      
}

void outputArray(int datas[], int nb){
          // output all pin value as php array
          Serial.print("(");
          for (int i = 0; i < nb; i++) {
            Serial.print(datas[i]);
             if (i != (nb -1)) Serial.print(",");
          }
          Serial.println(")");
}
