

var Arduino = {
	url:"spip.php?action=arduino", // ou #URL_ACTION_AUTEUR{arduino, ''}

	// lire toutes les entrees analogiques
	analogReadAll: function(){
		return this.get({op:"analogReadAll"});		
	},
	
	// lire une entree analogique donnee
	analogRead: function(pin){
		return this.get({op:"analogRead", pin:pin});		
	},
	
	// lire toutes les entrees numeriques
	digitalReadAll: function(){
		return this.get({op:"digitalReadAll"});		
	},
	
	// lire une entree numerique donnee
	digitalRead: function(pin){
		return this.get({op:"digitalRead", pin:pin});		
	},

	// ecrire une entree donnee
	analogWrite: function(pin, value){
		return this.set({op:"analogWrite", pin:pin, value:value});		
	},
	
	// ecrire une entree donnee
	digitalWrite: function(pin, value){
		return this.set({op:"digitalWrite", pin:pin, value:value});		
	},
	
	// $.getJSON est asynchrone, et ca ne va pas donc,
	// on se fait une fonction perso
	get: function(params) {
		data = $.ajax({
			type: "GET",
			url: this.url,
			data: params,
			dataType: "json",
			async: false
		}).responseText;
		return data = window["eval"]("(" + data + ")");
	},
	
	// set asynchrone pour l'ecriture
	set: function(params) {
		$.ajax({
			type: "GET",
			url: this.url,
			data: params,
			dataType: "json",
		});
	},	
};
